'use strict';
if (!window.console) window.console = {};
if (!window.console.memory) window.console.memory = function() {};
if (!window.console.debug) window.console.debug = function() {};
if (!window.console.error) window.console.error = function() {};
if (!window.console.info) window.console.info = function() {};
if (!window.console.log) window.console.log = function() {};


//Safari Margins-Fix
var ios = (/iphone|ipad|ipod/i.test(navigator.userAgent.toLowerCase()));

if (ios) {
    $('.pager .item').css('margin-right', '75px');
    $('.pager .item:nth-child(2)').css('margin-right', '77px');
    $('.pager .item:nth-child(3)').css('margin-right', '76px');
    $('meta[name=viewport]').attr('content',
        'width=device-width,initial-scale=1,maximum-scale=1.0');
}

var isSafari = Object.prototype.toString.call(window.HTMLElement).indexOf(
    'Constructor') > 0;

if (isSafari) {
    $('.pager .item').css('margin-right', '76px');
    $('.pager .item:nth-child(2)').css('margin-right', '77px');
    $('.pager .item:nth-child(3)').css('margin-right', '76px');
    $('.product-thumb .slick-slde').addClass('safari-fix');
}


// sticky footer
//-----------------------------------------------------------------------------
if (!Modernizr.flexbox) {
    (function() {
        var
            $pageWrapper = $('#page-wrapper'),
            $pageBody = $('#page-body'),
            noFlexboxStickyFooter = function() {
                $pageBody.height('auto');
                if ($pageBody.height() + $('#header').outerHeight() + $('#footer').outerHeight() <
                    $(window).height()) {
                    $pageBody.height($(window).height() - $('#header').outerHeight() -
                        $('#footer').outerHeight());
                } else {
                    $pageWrapper.height('auto');
                }
            };
        $(window).on('load resize', noFlexboxStickyFooter);
    })();
}
if (ieDetector.ieVersion == 10 || ieDetector.ieVersion == 11) {
    (function() {
        var
            $pageWrapper = $('#page-wrapper'),
            $pageBody = $('#page-body'),
            ieFlexboxFix = function() {
                if ($pageBody.addClass('flex-none').height() + $('#header').outerHeight() +
                    $('#footer').outerHeight() < $(window).height()) {
                    $pageWrapper.height($(window).height() + 30);
                    $pageBody.removeClass('flex-none');

                } else {
                    $pageWrapper.height('auto');
                }
            };
        ieFlexboxFix();
        $(window).on('load resize', ieFlexboxFix);
    })();
}

$(function() {

    // placeholder
    //-----------------------------------------------------------------------------
    $('input[placeholder], textarea[placeholder]').placeholder();

});

var availableTags = [
    "ActionScript",
    "AppleScript",
    "Asp",
    "BASIC",
    "C",
    "C++",
    "Clojure",
    "COBOL",
    "ColdFusion",
    "Erlang",
    "Fortran",
    "Groovy",
    "Haskell",
    "Java",
    "JavaScript",
    "Lisp",
    "Perl",
    "PHP",
    "Python",
    "Ruby",
    "Scala",
    "Scheme"
];

$(
    '.search-bar, .lang-select select, input[type=checkbox], input[type=radio], .dropdown-style'
).styler({});

$(".search-bar").autocomplete({
    source: availableTags
});

$('.js-show-langs').on('click', function() {
    //e.preventDefault();
    console.log('click');
    $(this).toggleClass('active');
    $('.langs').fadeToggle('fast');
});


//Search-header

$('.js-show-search').on('click', function(e) {
    e.preventDefault();

    if ($('.pop-up-contacts').hasClass('active')) {
        close_contacts();
    }

    $(this).toggleClass('active');
    $('.overlay').fadeToggle('fast');
    $('.search-wrap').fadeToggle(400);
    $('.search-bar').focus();
    $('.search-wrap input[type=submit]').toggle();
    $('#ui-id-1').attr('style', 'width:' + ($('.search-bar').outerWidth() + 5) +
        'px !important;');
});

$('.js-close-search').on('click', function(e) {
    e.preventDefault();
    closesearch()
});

function closesearch() {
    $('.search-wrap, .overlay, .search-wrap input[type=submit]').fadeOut('fast');
    $('.js-show-search').removeClass('active');
}

$('.js-close-mobile-search').click(function(e) {
    e.preventDefault();
    $('.mobile-search').find('form').trigger('reset');
});

$('.js-show-contacts').mouseenter(function(e) {
    e.preventDefault();
    $('.overlay').fadeIn('fast');

    if ($('.search-btn').hasClass('active')) {
        closesearch();
    }

    if ($('.pop-up-contacts').outerHeight() > ($(window).height() - 150)) {
        $('.height-fix').addClass('active').css({
            'height': $(window).outerHeight() - 100
        });
        $('.height-fix').niceScroll();
    }

    if ($('.pop-up-contacts').hasClass('active')) {
        return;
    } else {
        $('.pop-up-contacts').addClass('slideInRight animated active');
        setTimeout(function() {
            $('.pop-up-contacts').removeClass('slideInRight animated');
        }, 500);
    }

    $(this).addClass('active');
});

$('.js-close-pop-up').on('click', function(e) {
    e.preventDefault();
    close_contacts()
});

$('body').keydown(function(eventObject) {
    if (eventObject.which === 27) {
        close_contacts();
        closepop();
        closeGallery();
    }

});



function close_contacts() {
    if (!($('.menu-button').hasClass('active'))) {
        $('.overlay').fadeOut('fast');
    }
    if ($(window).outerWidth() < 480) {
        $('.pop-up-contacts').fadeOut('fast').removeClass('mobile-contact-form');
    } else {
        $('.pop-up-contacts').addClass('fadeOutRight animated');
        $('.pop-up-form input, textarea').removeClass('error');
        $('.pop-up-form').trigger('reset');
        $('.pop-up-form label.error').remove();
        setTimeout(function() {
            $('.pop-up-contacts').removeClass('fadeOutRight animated active');
            $('.js-show-contacts').removeClass('active');
        }, 500);
    }

    $(".thank-msg-l").slideUp(300);
    $("#contacts_form_2").slideDown(300); //back 
    $(".thank-msg-l").remove();
}

//Pop Up
$('.js-show-main-pop').click(function(e) {
    e.preventDefault();
    $('.overlay').fadeIn('fast');
    $('.pop-up-block').fadeIn('fast');
});

$('.js-close-pop').click(function(e) {
    e.preventDefault();
    closepop();
});

$('.overlay').click(function() {
    closepop();
    closesearch();
    close_contacts();
    close_mobile_menu();

    $(this).fadeOut('fast');
    if ($(window).outerWidth < 767) {
        hideMobFilter();
    }
});

$('.overlay').on('touchstart', function() {
    close_mobile_menu();
    $(this).fadeOut('fast');
});

function close_mobile_menu() {
    if ($('.menu-button').hasClass('active')) {
        $('.menu').slideToggle('fast')
        $('.menu-button').removeClass('active');
    }
}

function closepop() {
    $('.overlay').fadeOut('fast');
    $('.pop-up-block').fadeOut('fast').find('input[type=phone],input[type=name]');
    $('.pop-up-block input[type=phone],input[type=name]').removeClass('error');
    $('.pop-up-block').find('form').trigger('reset');
    $('.pop-up-block label.error').remove();

    $('.pop-up-block input[name=name]').attr('class', 'input-name'); //back
    $('.pop-up-block input[name=phone]').attr('class', 'input-phone');
};


//Mobile Contacts

$('.js-show-contacts-mobile').click(function(e) {
    e.preventDefault();
    if ($('.pop-up-contacts').outerHeight() > $(window).height()) {
        $('.height-fix').addClass('active').css('height', $(window).outerHeight() -
            50);
        $('.height-fix').niceScroll();
    }
    if ($(window).outerWidth() < 480) {
        $('.pop-up-contacts').fadeIn('fast').addClass('mobile-contact-form');
    } else {
        $('.pop-up-contacts').addClass(
            'bounceInRight animated active mobile-contact-form');
        setTimeout(function() {
            $('.pop-up-contacts').removeClass('bounceInRight animated');
        }, 800);
    }


});

$('.js-show-menu').on('click', function(e) {
    e.preventDefault();
    if ($(this).hasClass('active')) {
        $(this).removeClass('active');
    } else {
        $(this).addClass('active');
    }

    $('.menu').slideToggle();
    $('.overlay').fadeToggle('fast');
});

var time = 3000;
var itemHeight;

var itemWidth;
var lineWidth;
var slides = $('.carusel-wow .owl-page').length;


var owl_page_len;
var minus = 15;
var interval;
var counter = 1;
var pos = 2;
var speed = 0.4;


function init() {
    var slidesitem = ($('.items .slide').length - 4);
    var i = 0;

    while (i < slidesitem) {
        $('.line .pager').append('<div class="item"><div class="block">&nbsp</div></div>');
        console.log(i);
        i++;
    }



    $('.line').appendTo($('.owl-controls'));
    itemWidth = $('.owl-dots .owl-dot').outerWidth() + 80;
    lineWidth = itemWidth * ($('.owl-dots .owl-dot').length - 1);
    $('.line').css('width', (lineWidth + 20) + 'px');
    $('.owl-dots .owl-dot').each(function(i) {
        $(this).attr('data', i);
    });
    var num = 0;
    $('.owl-stage .owl-item').each(function() {
        if ($(this).hasClass('cloned')) {} else {
            $(this).addClass('thumb ' + num);
            $(this).attr('data', num);
            num++;
        }
    });
    autoplaywidth()
}


var i = 0;
var owlAutoplayTimeout = 5000
var options = {
    loop: true,
    nav: true,
    dots: true,
    items: 1,
    onInitialized: init,
    itemsTablet: [992, 1],
    itemsDesktop: [],
    itemsMobile: [479, 1],
    smartSpeed: 800,
    // slideSpeed: 4500,
    // paginationSpeed: 4500,
    // rewindSpeed: 1000,
    autoplay: true,
    autoplayTimeout: owlAutoplayTimeout
};

var carusel = $('.owl-carousel').owlCarousel2(options).data('owlCarousel');


function autoplaywidth() {
    $('.owl-controls .pager').css('width', '20px').animate({
        width: "+=" + (itemWidth - 2)
    }, owlAutoplayTimeout + 1000);
};


$('.owl-carousel').on('changed.owl.carousel', function() {
    var itemWidth = $('.owl-pagination .owl-page').outerWidth() + 100;
    $('.owl-controls .pager').animate({
        width: $('.owl-dot.active').attr('data') * itemWidth + 20
    }, 100);
    //Переход к первому
    if ($('.slider-wrap .owl-dot:last-child').hasClass('active')) {
        $('.owl-controls .pager').animate({
            width: 20
        }, 5000);
        return
    }

    $('.owl-controls .pager').animate({
        width: "+=" + itemWidth
    }, owlAutoplayTimeout);
});

$('.slider-wrap .owl-dot,.owl-next,.owl-prev').click(function() {
    if (!($(this).hasClass('active'))) {
        $('.owl-controls .pager').stop(true, true).animate({
            width: $('.owl-dot.active').attr('data') * itemWidth + 20
        }, 10);
    }
});



//Thumbs help
//Show Tumbnails of Next Slide

$('.owl-next').mouseenter(function() {
    var number = (Number($('.active.thumb').attr("data")) + 1),
        slidelenght = ($('.thumb').length);
    if (number == (($('.thumb').length))) {
        $('.thumb.0').find('img').clone().addClass('clone-tumb right-tumb').appendTo(
            $('.slider-wrap .owl-nav'));
    } else {
        $('.thumb.' + number).find('img').addClass('123').clone().addClass(
            'clone-tumb right-tumb').appendTo($('.slider-wrap .owl-nav'));
    }
    if ($('.slider-wrap .cloned:nth-last-of-type(2)').hasClass('active')) {
        $('.thumb.' + (slidelenght - 3)).find('img').clone().addClass(
            'clone-tumb right-tumb').appendTo($('.slider-wrap .owl-nav'));
    }
});

$('.owl-next').mouseleave(function() {
    $('.clone-tumb').remove();
});

//Show Tumbnails of Prev Slide
$('.owl-prev').mouseenter(function() {
    var number = (Number($('.active.thumb').attr("data"))),
        slidelenght = ($('.thumb').length);
    if (number == 0) {
        $('.slider-wrap .owl-item:nth-child(' + (slidelenght + 2) + ')').find(
            'img').clone().addClass('clone-tumb left-tumb').appendTo($(
            '.slider-wrap .owl-nav'));
    } else {
        $('.thumb.' + (number - 1)).find('img').clone().addClass(
            'clone-tumb left-tumb').appendTo($('.slider-wrap .owl-nav'));
    }

    if ($('.slider-wrap .cloned:nth-child(2)').hasClass('active')) {
        $('.thumb.' + (slidelenght - 2)).find('img').clone().addClass(
            'clone-tumb left-tumb').appendTo($('.slider-wrap .owl-nav'));
    }
});

$('.owl-prev').mouseleave(function() {
    $('.clone-tumb').remove();
});


//Companys carousel

$('.copmanys-slider').owlCarousel2({
    loop: true,
    items: 6,
    nav: true,
    navText: false,
    responsiveClass: true,
    responsive: {
        0: {
            items: 1,
            nav: false
        },
        480: {
            items: 2,
            nav: false
        },
        640: {
            items: 2,
            nav: true,
        },
        767: {
            items: 3,
            nav: true,
        },
        1000: {
            items: 5,
            nav: true,
        },
        1500: {
            items: 6,
            nav: true,
        }
    }
});

$('.carousel').owlCarousel2({
    smartSpeed: 800,
    loop: true,
    items: 2,
    nav: true,
    navText: false,
    responsiveClass: true,
    responsive: {
        0: {
            items: 1,
            nav: false
        },
        650: {
            items: 2,
            nav: false
        },
        992: {
            items: 3,
            nav: true,
        },
        1200: {
            items: 2,
            nav: true,
        },

    }
});


$('.inside-carousel').owlCarousel2({
    items: 1
})


/*
 *  Google Map
 */

google.maps.event.addDomListener(window, "load", initMap);

function initMap() {
    var map;
    var myLatlng = new google.maps.LatLng(49.41575, 27.017900);


    map = new google.maps.Map(document.getElementById('map'), {
        center: new google.maps.LatLng(49.415898, 27.019500),
        zoom: 17,
        scrollwheel: false,
    });

    var image = new google.maps.MarkerImage('img/map-icon.png',
        new google.maps.Size(50, 60)
    );


    var marker = new google.maps.Marker({
        icon: image,
        position: myLatlng,
    });

    if ($(window).outerWidth() < 767) {
        marker.setMap(null);
    } else {
        marker.setMap(map);
    }
};

$('.js-show-feedback').click(function(e) {
    e.preventDefault();
    $('.slide-form').fadeIn('fast');
    if ($(window).outerWidth() < 768) {
        $('.feedback-form-slide').addClass('active mobile');
        $('.slide-form').find('.feedback-form-slide').fadeIn('fast');
        $('.slide-form').addClass('active-mobile');
        $(this).removeClass('js-show-feedback').addClass('submit');
    } else {
        $('.feedback-form-slide').addClass('active');
        $('.slide-form').css('left', '-415px');
        $(this).removeClass('js-show-feedback').addClass('submit');
    }
});


$('.js-close-foot-pop-up').click(function(e) {
    e.preventDefault();
    if ($(window).outerWidth() < 768) {
        $('.slide-form').find('.feedback-form-slide').fadeOut('fast');
        $('.slide-form').removeClass('active-mobile');
        $('.pop-up-form label.error').remove();
        $('.pop-up-form input, textarea').removeClass('error').val('');
        $('.pop-up-form').trigger('reset');
        $('.feedback-form-slide').removeClass('active mobile');
        $('.foot-btn').addClass('js-show-feedback').removeClass('submit');
    } else {
        $('.pop-up-form label.error').remove();
        $('.pop-up-form input, textarea').removeClass('error');
        $('.pop-up-form').trigger('reset');
        $('.feedback-form-slide').removeClass('active mobile');
        $('.foot-btn').addClass('js-show-feedback').removeClass('submit');
        setTimeout(function() {
            $('.feedback-form-slide').css('left', '0px').css('z-index', '1');
            $(".thank-msg-l").slideUp(300);
            $("#contacts_form").slideDown(300); //back
            $(".thank-msg-l").remove();
        }, 100);
    }
});


//Animation numbers
$('.paralaks').one('inview', function(event, visible) {
    if (visible == true) {
        numbers();
    }
});

function numbers() {
    $('.numbers').css('opacity', '1');
    $('.js-numbers').counterUp({
        time: 2000
    });
}

//Scroll-top

$(window).scroll(function() {
    if ($(this).scrollTop() > 100) {
        $('.back-top').fadeIn();
        //$('header .container-fluid').addClass('fixed animated fadeInDown');

    } else {
        $('.back-top').fadeOut();
        //$('header .container-fluid').removeClass('fixed animated fadeInDown');
    }
});

$('.js-scroll-top').click(function(e) {
    e.preventDefault();
    $('html, body').animate({
        scrollTop: 0
    }, 800);
});

//Equial Height
function equal() {
    $('.product-list').find('.col-md-3').matchHeight({
        property: 'height'
    });

}

function equalart() {
    $('.carousel-inside').find('.item').matchHeight({
        // property: 'height'
    });
    $('.carousel').find('.item').matchHeight({
        // property: 'height'
    });
}

$(window).load(function() {
    equal();
    equalart();

    $('.page-wrapper').fadeTo('fast', 1);
    setTimeout(function() {
        $('.brand-logo > img').addClass('animated-logo fadeIn').css(
            'display',
            'block');
    }, 350);
});

$.fn.matchHeight._throttle = 1000;

$(window).on('resize', function() {
    equalart();
})


$('.pop-up-form').find('input, textarea').keydown(function() {
    $(this).removeClass('error');
    $('#' + $(this).attr('name') + '-error').remove();
});

$('.pop-up-block').find('input, textarea').keydown(function() {
    $(this).removeClass('error');
    $('#' + $(this).attr('name') + '-error').remove();
});


$("input[type=phone]").keydown(function(event) {
    if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 32 ||
        event.keyCode == 187 || event.keyCode == 189 || event.keyCode == 13 ||
        event.keyCode == 107 || event.keyCode == 109 ||
        (event.keyCode == 65 && event.ctrlKey === true) ||
        (event.keyCode >= 35 && event.keyCode <= 39)) {
        return;
    } else {
        if ((event.keyCode < 40 || event.keyCode > 57) && (event.keyCode < 96 ||
                event.keyCode > 105)) {
            event.preventDefault();
        }
    }
});

$(window).load(function() {
    if ($('html').hasClass('lte-ie-11') || $('html').hasClass('lte-ie-10')) {
        $('.copmanys-slider img').each(function() {
            $(this).wrap('<div style="display:inline-block;width:' + this.width +
                'px;height:' + this.height + 'px;">').clone().addClass(
                'gotcolors').css({
                'position': 'absolute',
                'opacity': 0
            }).insertBefore(this);
            this.src = grayscale(this.src);
        }).animate({
            opacity: 1
        }, 500);
    }
});

$(document).ready(function() {
    $(".copmanys-slider a").hover(
        function() {
            $(this).find('.gotcolors').stop(true, true).animate({
                opacity: 1
            }, 200);
        },
        function() {
            $(this).find('.gotcolors').stop(true, true).animate({
                opacity: 0
            }, 500);
        }
    );
});

function grayscale(src) {
    var supportsCanvas = document.createElement('canvas').getContext;

    if (supportsCanvas) {
        var canvas = document.createElement('canvas'),
            context = canvas.getContext('2d'),
            imageData, px, length, i = 0,
            gray,
            img = new Image();
        img.crossOrigin = "Anonymous";

        img.src = src;
        canvas.width = img.width;

        canvas.height = img.height;
        context.drawImage(img, 0, 0);


        imageData = context.getImageData(0, 0, canvas.width, canvas.height);
        px = imageData.data;
        length = px.length;

        for (; i < length; i += 4) {
            gray = px[i] * .3 + px[i + 1] * .59 + px[i + 2] * .11;
            px[i] = px[i + 1] = px[i + 2] = gray;
        }

        context.putImageData(imageData, 0, 0);
        return canvas.toDataURL();
    } else {
        return src;
    }
}

$('.product').each(function() {
    var len = $(this).find('li').length;
    if (len <= 5) {
        //$(this).find('.list-style').css({ "-webkit-column-count": "1","-moz-column-count": "1","column-count": "1"});
        $(this).find('.list-style').attr('style',
            '-webkit-column-count: 1 !important  ; -moz-column-count: 1 !important ;column-count: 1 !important;'
        );
    } else if ((len > 5) && (len < 11)) {
        $(this).find('.list-style').css({
            "-webkit-column-count": "2",
            "-moz-column-count": "2",
            "column-count": "2"
        });
    } else {
        $(this).find('.list-style').css({
            "-webkit-column-count": "3",
            "-moz-column-count": "3",
            "column-count": "3"
        });
    }
});

//Hover on IOS fix

document.addEventListener("touchstart", function() {}, false);

//Pagination responsive fix

var pagination = $('.pagination');

function pagin() {
    if ($(window).outerWidth() < 580) {
        pagination.find('.prev').html('<');
        pagination.find('.next').html('>');
    } else {
        // pagination.find('.prev').html('Предыдущая');
        // pagination.find('.next').html('Следующая');//back
    }
}

$(window).on('load resize', function() {
    pagin();
});

$('body').bind('copy paste cut drag drop', function(e) {
    e.preventDefault();
});


//Type Files Fix


//NEW SCRIPTS

//Akardeion

$('.js-open-ak').on('click', function(e) {
    e.preventDefault();
    // $('.ak-body-parent ul li').each(function(){
    //   if ($(this).hasClass('open'))
    //   {
    //     $(this).removeClass('open').find('.ak-body').slideUp('fast');    
    //   }
    // })
    if ($(this).parent().hasClass('open')) {
        $(this).parent().removeClass('open').find('.ak-body').slideUp('fast');
    } else {
        $('.ak-body-parent').find('.open').removeClass('open').find('.ak-body').slideUp('fast');
        $(this).parent().addClass('open').find('.ak-body').slideDown('fast');
    }
});

//*******
//Slick POP detail gallery
//*******


//*******
//Slick detail page
//*******


$('.product-slider').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    asNavFor: '.product-thumb',
    swipe: false,
    // infinite: false,
    responsive: [{
        breakpoint: 991,
        settings: {
            // infinite: false,
            swipe: true
        }
    }]

});

$('.product-thumb').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    asNavFor: '.product-slider',
    focusOnSelect: true,
    vertical: true,
    swipe: false,
    // infinite: false,
    responsive: [{
            breakpoint: 991,
            settings: {
                vertical: false,
                slidesToShow: 4,
                slidesToScroll: 1,
                swipe: true
            }
        }, {
            breakpoint: 480,
            settings: {
                vertical: false,
                speed: 400,
                slidesToShow: 3,
                swipe: true,
                swipeToSlide: true
            }
    }]
});
// $('.product-thumb').find('.slick-current').addClass('active');

// $('.slick-prev').on('click', function() {
//     $('.product-thumb').find('.active').removeClass('active');
//     $('.product-thumb').find('.slick-current').addClass('active');
// });


// $('.slick-next').on('click', function() {
//     $('.product-thumb').find('.active').removeClass('active');
//     $('.product-thumb').find('.slick-current').addClass('active');
// });


// $('.slick-slider').on('afterChange', function(event, slick, currentSlide, nextSlide) {
//     var activebig = $('.product-slider').find('.slick-current.slick-active').attr('data-slick-index');
//     $('.product-thumb').find('.slick-slide').removeClass('active slick-current');
//     $('.product-thumb img[data-slick-index=' + activebig + ']').addClass('active slick-current');
//     if ($(window).outerWidth() < 991) {
//         $('.product-thumb .slick-slide').removeAttr('attrName-id');
//         $('.product-thumb .slick-active').each(function(i) {
//             $(this).attr('attrName-id', i);
//         });
//     }
// });

// $('.slick-slide').click(function() {
//     $('.product-thumb').find('.active').removeClass('active');
//     $(this).addClass('active');

//     if ($(window).outerWidth() < 991) {
//         if ($(this).attr('attrName-id') == 1) {
//             $('.product-thumb').slick('slickGoTo', $(this).attr('data-slick-index') - 3);
//         } else if ($(this).attr('attrName-id') == 0) {
//             console.log('zero');
//             $('.product-thumb').slick('slickGoTo', -3);
//         } else {
//             $('.product-thumb').slick('slickGoTo', $(this).attr('data-slick-index') + 1);
//         }
//     }

// });

// $('.product-thumb .slick-active').each(function(i) {
//     if ($(window).outerWidth() < 991) {
//         $(this).attr('attrName-id', i);
//     }

// });

//Tabs

var tablink = $('.detail-tab li a'),
    tabbody = $('.tabs-body');

tablink.on('click', function(e) {
    console.log('123');
    e.preventDefault();
    var findlink = $(this).attr('href');

    if ($(this).hasClass('active')) {
        return
    } else {
        tablink.removeClass('active');
        tabbody.find('.active').removeClass('active');
        $(this).addClass('active');
        tabbody.find(findlink).addClass('active');
    }
});



//

$('.detail-table').DataTable({
    paging: false,
    searching: false
});

//Mobile Filter
var filterlist = $('.filter-body ul');

$('.js-show-mobile-filter').on('click', function(e) {

    e.preventDefault();
    if ($(this).hasClass('active')) {
        hideMobFilter();
    } else {
        showMobFilter();
    }


});

function showMobFilter() {
    $('.overlay').fadeIn('fast');
    filterlist.slideDown('fast');
    $('.js-show-mobile-filter').addClass('active');
    if ($(window).outerWidth() <= 380) {
        $('.call-to-action, .back-top').fadeOut('fast');
    }
}

function hideMobFilter() {
    $('.overlay').fadeOut('fast');
    filterlist.slideUp('fast');
    $('.js-show-mobile-filter').removeClass('active');
    if ($(window).outerWidth() < 380) {
        $('.call-to-action, .back-top').fadeIn('fast');
    }
}

//Mobile Catalog

$('.js-show-catalog').on('click', function(e) {
    e.preventDefault();
    var productBody = $('.ak-body-parent');
    var height = $('.menu-catalog').offset().top;

    if ($(this).hasClass('active')) {
        productBody.slideUp('fast');
        $(this).removeClass('active')
    } else {
        $(this).addClass('active')
        productBody.slideDown('fast');
        $('html body').delay(250).animate({
            scrollTop: height - 70
        }, '1000');
    }
});


$(window).on('load', function() {
    fixed();
});
$(window).on('scroll', function() {
    fixed();
});


function fixed() {
    if ($(window).outerWidth() < 780) {
        if ($(window).scrollTop() > 220) {
            $('.menu-catalog').addClass('fixed');
            $('.production-catalog .catalog-body .title').addClass('fixed');
            $('.ak-body-parent').addClass('fixed');

            $('.ak-body-parent > ul').css({
                'overflow-y': 'scroll',
                'height': $(window).outerHeight() - 140
            });
        } else {
            $('.menu-catalog').removeClass('fixed');
            $('.production-catalog .catalog-body .title').removeClass('fixed');
            $('.ak-body-parent').removeClass('fixed');
        }
    }
}

//Detail slider pop up
$('.product-slider,.zoom-icon').on('click', function(e) {
    if ($(window).outerWidth() > 975) {
        $('.pop-up-detail').fadeIn('fast');
        $('.pop-up-detail').css('top', $(window).scrollTop() + 100);
        $('.overlay-black').fadeIn('fast');

        $('.gallery .main-img').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            speed: 800,
            asNavFor: '#syns2',
        });

        $('#sync2').slick({
            slidesToShow: 8,
            slidesToScroll: 1,
            speed: 800,
            asNavFor: '.main-img',
            focusOnSelect: true,
            swipeToSlide: true,
        });
    }
});
$('.js-show-zoom').on('click', function(e) {
    var slideIndex = $(this).attr('data-slick-index');
    e.preventDefault();
    if (!($(window).outerWidth() < 974)) {
        $('.pop-up-detail').fadeIn('fast');
        $('.pop-up-detail').css('top', $(window).scrollTop() + 100);
        $('.overlay-black').fadeIn('fast');
        setTimeout(function(){
          $('#sync2').slick('slickGoTo',slideIndex);
        },40);
    }
});

$('.js-close-gallery').on('click', function(e) {
    e.preventDefault();
    closeGallery()
})
$('.overlay-black').on('click', function() {
    closeGallery();
})

function closeGallery() {
    $('.pop-up-detail').fadeOut('fast');
    $('.overlay-black').fadeOut('fast');
}

